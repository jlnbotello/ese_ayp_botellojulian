var annotated_dup =
[
    [ "delta_t", "structdelta__t.html", "structdelta__t" ],
    [ "gpioCfg_t", "structgpio_cfg__t.html", "structgpio_cfg__t" ],
    [ "gpioPin_t", "structgpio_pin__t.html", "structgpio_pin__t" ],
    [ "ledMatrix_t", "structled_matrix__t.html", "structled_matrix__t" ],
    [ "max7219_t", "structmax7219__t.html", "structmax7219__t" ],
    [ "nrf24l01_t", "structnrf24l01__t.html", "structnrf24l01__t" ],
    [ "spiConfig_t", "structspi_config__t.html", "structspi_config__t" ],
    [ "spiDevice_t", "structspi_device__t.html", "structspi_device__t" ],
    [ "sspCfg_t", "structssp_cfg__t.html", "structssp_cfg__t" ],
    [ "stagedPoint_t", "structstaged_point__t.html", "structstaged_point__t" ]
];