var searchData=
[
  ['rcv_5ffr_5fprx',['rcv_fr_PRX',['../group___n_r_f24_l01.html#ga8e5dd77eaaf88bdb03195f620330906e',1,'rcv_fr_PRX():&#160;nrf24l01.c'],['../group___n_r_f24_l01.html#ga92af9df0e60258e291c4971da5aef934',1,'rcv_fr_PRX():&#160;nrf24l01.c']]],
  ['rcv_5ffr_5fptx',['rcv_fr_PTX',['../group___n_r_f24_l01.html#ga6eeb85a155d676ef147f9b5491738905',1,'rcv_fr_PTX():&#160;nrf24l01.c'],['../group___n_r_f24_l01.html#gaddb0ff79c3c3eb6123bee45889965cc3',1,'rcv_fr_PTX():&#160;nrf24l01.c']]],
  ['read_5fswitches',['Read_Switches',['../group__switches.html#ga062112e932fd77a1fa64764587b1df98',1,'Read_Switches(void):&#160;switch.c'],['../group__switches.html#ga062112e932fd77a1fa64764587b1df98',1,'Read_Switches(void):&#160;switch.c']]],
  ['red_5fled',['RED_LED',['../group__leds.html#gga1f3289eeddfbcff1515a3786dc0518faaedcf3ad9edba345343e3d99b9d27d5a9',1,'led.h']]],
  ['resettick',['ResetTick',['../group__led__mat.html#gaa49af1a0dcfe233c5f64f338c0b87045',1,'led_matrix.c']]],
  ['rgb_5fb_5fled',['RGB_B_LED',['../group__leds.html#gga1f3289eeddfbcff1515a3786dc0518faaa7c94ad61c92a8fea11db9b9a4391940',1,'led.h']]],
  ['rgb_5fg_5fled',['RGB_G_LED',['../group__leds.html#gga1f3289eeddfbcff1515a3786dc0518faa50aa0cc525d6fa968fa5fb5bf700cdd4',1,'led.h']]],
  ['rgb_5fr_5fled',['RGB_R_LED',['../group__leds.html#gga1f3289eeddfbcff1515a3786dc0518faae497ffcd7c6d3b74056ead0449d92f55',1,'led.h']]],
  ['rot_5f0_5fcw',['ROT_0_CW',['../group__led__mat.html#gga8fb070c465918a95ee2d3b296fe65e74a9d77c4964b97af8590d8c7cd86be1a5f',1,'led_matrix.h']]],
  ['rot_5f180_5fcw',['ROT_180_CW',['../group__led__mat.html#gga8fb070c465918a95ee2d3b296fe65e74ac50c1e5c993750be5c8d518a48e9e1cf',1,'led_matrix.h']]],
  ['rot_5f270_5fcw',['ROT_270_CW',['../group__led__mat.html#gga8fb070c465918a95ee2d3b296fe65e74a1326c3ce26fb0b9835998d315cb5592c',1,'led_matrix.h']]],
  ['rot_5f90_5fcw',['ROT_90_CW',['../group__led__mat.html#gga8fb070c465918a95ee2d3b296fe65e74ab0ff4aa7e9875eb64a828407d29b5716',1,'led_matrix.h']]],
  ['rst_5fpoint',['RST_POINT',['../group__max7219.html#gga2fa196c9c3c1c7fadaa9c5687b7c4b34a5825cc59a8a5214a39a513a50fd650ff',1,'max7219.h']]]
];
