var uart__debug_8h =
[
    [ "UartInit", "group__debug__uart.html#ga61f5dee7d3d128cc710e664a198d0904", null ],
    [ "UartPrint", "group__debug__uart.html#ga5df185bb2a3728b46013dc8941d6d86a", null ],
    [ "UartPrintMsgAndBin", "group__debug__uart.html#gaa4673035300c4507f0692900f1dfb06d", null ],
    [ "UartPrintMsgAndDec", "group__debug__uart.html#ga851f156d02a94e321f5810f1ec2c2ebd", null ],
    [ "UartPrintMsgAndHex", "group__debug__uart.html#gab65aa64d3ec8cfef7b4cee3d2ee8963f", null ],
    [ "UartReadBlocking", "group__debug__uart.html#ga9130fbb232e260d273771af56a2a373a", null ],
    [ "UartSendBlocking", "group__debug__uart.html#ga75a5dbcd89e86dc33090c7aa3d4d504a", null ],
    [ "Uint32ToBin", "group__debug__uart.html#ga5f4fc49c6952df97b6275d16f3b66354", null ],
    [ "Uint32ToDec", "group__debug__uart.html#ga9aee49bad8edfa6b270d9560ce70c036", null ],
    [ "Uint32ToHex", "group__debug__uart.html#ga7c6ff2966e572e7532d8e9cd722a39b2", null ]
];