/*==================[inclusions]=============================================*/

#include "system_init.h"
#include "chip.h"

/*==================[external data definition]===============================*/

const uint32_t ExtRateIn = 0;  /**< External frequency clock injected to the microcontroller. Not used in the CIAAs  */
const uint32_t OscRateIn = 12000000; /**< Frequency of the internal oscillator incorporated in theCIAA-NXP. */

/*==================[external functions definition]=========================*/

void SystemInit(void)
{	//CLOCK
	Chip_SetupXtalClocking();
	SystemCoreClockUpdate();
	//FPU
    fpuInit();
}

void SysTickInit(uint32_t ticks_per_sec){
	SysTick_Config ( SystemCoreClock / ticks_per_sec);
}


