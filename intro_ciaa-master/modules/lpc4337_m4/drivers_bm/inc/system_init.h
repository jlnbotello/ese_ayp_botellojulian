#ifndef MODULES_LPC4337_M4_DRIVERS_BM_INC_SYSTEM_INIT_H_
#define MODULES_LPC4337_M4_DRIVERS_BM_INC_SYSTEM_INIT_H_

/*==================[inclusions]=============================================*/

#include "stdint.h"

/*==================[external functions declaration]=========================*/

void SystemInit(void);

void SysTickInit(uint32_t ticks_per_sec);

#endif /* MODULES_LPC4337_M4_DRIVERS_BM_INC_SYSTEM_INIT_H_ */
