/* Copyright 2018, Eduardo Filomena - Gonzalo Cuenca
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Blinking Bare Metal example source file
 **
 ** This is a mini example of the CIAA Firmware.
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */

/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup Baremetal Bare Metal example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 *
 */	

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * yyyymmdd v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "system_init.h"
#include "gpio_hal.h"
#include "switch.h"
#include "led.h"
#include "stopwatch.h"
#include "stdlib.h"
#include "chip.h"

/* IN THIS TEST EXAMPLE THE NUMBER OF PIN INTERRUPTION MATCH WITH GPIO NUMBER. THIS NOT MANDATORY */

#define PIN_INT_0 0
#define PIN_INT_1 1
#define PIN_INT_2 2
#define PIN_INT_3 3


/*ISR for GPIO0*/
void GPIO0_IRQHandler(){
	NVIC_DisableIRQ( PIN_INT0_IRQn);

	GpioInterrupt(PIN_INT_0);


	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(PIN_INT_0));
	NVIC_ClearPendingIRQ( PIN_INT0_IRQn);
	NVIC_EnableIRQ( PIN_INT0_IRQn);
}

/* ------------ Function of  NVIC ----------------*/

/*ISR for GPIO1*/
void GPIO1_IRQHandler(){
	NVIC_DisableIRQ( PIN_INT1_IRQn);

	GpioInterrupt(PIN_INT_1);

	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(PIN_INT_1));
	NVIC_ClearPendingIRQ( PIN_INT1_IRQn);
	NVIC_EnableIRQ( PIN_INT1_IRQn);
}

//*ISR for GPIO2*/
/*
 * DON'T use Chip_PININT_ClearIntStatus function.
 * It changes the active interrupt level to "clear interrupt".
 * The interrupt will end when the pin externally reaches to inactive interrupt level
 *
 */
void GPIO2_IRQHandler(){
	NVIC_DisableIRQ( PIN_INT2_IRQn);
	GpioInterrupt(PIN_INT_2);
	NVIC_EnableIRQ( PIN_INT2_IRQn);
}

/*ISR for GPIO3*/
/*
 * SAME AS ABOVE about Chip_PININT_ClearIntStatus
 */
void GPIO3_IRQHandler(){
	NVIC_DisableIRQ( PIN_INT3_IRQn);
	GpioInterrupt(PIN_INT_3);
	NVIC_EnableIRQ( PIN_INT3_IRQn);
}

/* ------------ Callback functions ----------------*/


void TestCallback0(){
	Led_On(RGB_B_LED);
	StopWatch_DelayMs(2000);
	Led_Off(RGB_B_LED);
}

void TestCallback1(){
	Led_On(RED_LED);
	StopWatch_DelayMs(2000);
	Led_Off(RED_LED);
}

void TestCallback2(){
	Led_Toggle(YELLOW_LED);
	StopWatch_DelayMs(500); // Because it will be active until the pin goes to inactive interrupt level
}

void TestCallback3(){
	Led_Toggle(GREEN_LED);
	StopWatch_DelayMs(500); // Because it will be active until the pin goes to inactive interrupt level
}

/**
 * @note This test example configures 4 INPUTS and 2 OUTPUTS
 *	Each input has an associated callback
 */
int main(void){

	SystemInit();
	Init_Leds();
	Init_Switches();
	Led_On(GREEN_LED);
	StopWatch_Init();

	GpioInit();

	/* Rising Edge Sensitive */
	gpioPin_t gpio0;
	gpio0.n=GPIO_0;
	gpio0.dir=GPIO_IN_PULLDOWN;
	GpioConfig(&gpio0);
	GpioInterruptConfig(&gpio0,GPIO_IRQ_EDGE_RISE,PIN_INT_0,TestCallback0);

	/* Falling Edge Sensitive */
	gpioPin_t gpio1;
	gpio1.n=GPIO_1;
	gpio1.dir=GPIO_IN_PULLUP;
	GpioConfig(&gpio1);
	GpioInterruptConfig(&gpio1,GPIO_IRQ_EDGE_FALL,PIN_INT_1,TestCallback1);

	/* High Level Sensitive */
	gpioPin_t gpio2;
	gpio2.n=GPIO_2;
	gpio2.dir=GPIO_IN_PULLDOWN; // IMPORTANT! If not PULLDOWN the interruption will be active forever
	GpioConfig(&gpio2);
	GpioInterruptConfig(&gpio2,GPIO_IRQ_LEVEL_HIGH,PIN_INT_2,TestCallback2);

	/* Low Level Sensitive */
	gpioPin_t gpio3;
	gpio3.n=GPIO_3;
	gpio3.dir=GPIO_IN_PULLUP; // IMPORTANT!  If not PULLUP the interruption will be active forever
	GpioConfig(&gpio3);
	GpioInterruptConfig(&gpio3,GPIO_IRQ_LEVEL_LOW,PIN_INT_3,TestCallback3);

	/* OUTPUTS */
	gpioPin_t gpio4;
	gpio4.n=GPIO_4;
	gpio4.dir=GPIO_OUT;
	gpio4.init_st=GPIO_HIGH;
	GpioConfig(&gpio4);

	gpioPin_t gpio5;
	gpio5.n=GPIO_5;
	gpio5.dir=GPIO_OUT;
	gpio5.init_st=GPIO_HIGH;
	GpioConfig(&gpio5);

	while(1){
		uint8_t key=Read_Switches();

			/*Disable and Enable when there is no ISR running*/
			if (key == 1) {

				NVIC_DisableIRQ(PIN_INT2_IRQn);

			}
			if (key == 2) {

				NVIC_EnableIRQ(PIN_INT2_IRQn);
			}
			if(key==4){

				NVIC_DisableIRQ( PIN_INT3_IRQn);

			}
			if(key==8){

				NVIC_EnableIRQ( PIN_INT3_IRQn);
			}

	}

	return 0;


}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

