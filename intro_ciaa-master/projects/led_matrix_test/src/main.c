/* Copyright 2018, Eduardo Filomena - Gonzalo Cuenca
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Blinking Bare Metal example source file
 **
 ** This is a mini example of the CIAA Firmware.
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */

/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup Baremetal Bare Metal example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 *
 */	

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * yyyymmdd v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "system_init.h"
#include "led_matrix.h"
#include "led.h"
#include "switch.h"
#include "stopwatch.h"

#define PART1

void SysTick_Handler(void){
	static uint32_t cnt = 0;
	cnt = cnt % 333;
	if (cnt == 0){
		Led_Toggle(GREEN_LED);
#ifdef PART2
		MatrixTick();
#endif
	}
	cnt ++;
}

#define CHANGE_DELAY 100

int main(void) {

	SystemInit();
	Init_Leds();
	Init_Switches();
	StopWatch_Init();
	SysTick_Config(SystemCoreClock / 1000);

	max7219_t max7219;
	Max7219Init(&max7219, GPIO_0, max7219_spi_default_cfg);

	ledMatrix_t mat;
	MatrixInit(&mat, max7219, ROT_270_CW);

	//arrow to pointing (x,y)=(8,8) led
	MatrixSetImage(&mat, 0xf0c0a09008040201);
	MatrixUpdate(&mat);

#ifdef PART2
	uint8_t swap_flag = 0;
	MatrixStagePoint(&mat,1,1);
#endif

	uint8_t key = 0;
	while (1) {
		key = Read_Switches();
#ifdef PART1
		/*Rotates image already set -90 degrees clockwise*/
		if(key==1) {
			MatrixRotate(&mat,ROT_270_CW);
			MatrixUpdate(&mat);
		}
		/*Rotates image already set +90 degrees clockwise*/
		if(key==2) {
			MatrixRotate(&mat,ROT_90_CW);
			MatrixUpdate(&mat);
		}
		/* Sets a happy face created on https://xantorohara.github.io/led-matrix-editor/ */
		if(key==4) {
			uint64_t happy_face = 0x3c42bdc381a5423c;
			MatrixSetImage(&mat, happy_face);
			MatrixUpdate(&mat);

		}
		/* Sets a sad face created on https://xantorohara.github.io/led-matrix-editor/ */
		if(key==8) {
			uint64_t sad_face =  0x3c42c3bd81a5423c;
			MatrixSetImage(&mat, sad_face);
			MatrixUpdate(&mat);
		}

#endif

#ifdef PART2
		/* Decrements X or Y based on swap_flag */
		if (key == 1) {

			if (swap_flag == 0) {
				MatrixMoveStagedPoint(&mat, X_MINUS_1);
			} else {
				MatrixMoveStagedPoint(&mat, Y_MINUS_1);
			}

		}
		/* Increments X or Y based on swap_flag */
		if (key == 2) {

			if (swap_flag == 0) {
				MatrixMoveStagedPoint(&mat, X_PLUS_1);
			} else {
				MatrixMoveStagedPoint(&mat, Y_PLUS_1);
			}

		}
		/* Toggles swap_flag */
		if (key == 4) {

			swap_flag = !swap_flag;

		}
		/* Changes the state of the staged point */
		if (key == 8) {

			MatrixTogStagePoint(&mat);

		}
		/*WARNING: don't change the other points of the matrix during staged point.
		 * Restore  value will out of date on 'unstage'.
		 */
		MatrixOnTickUpdateSP(&mat); /*Executes the changes on tick*/
		MatrixUpdate(&mat);
#endif
		StopWatch_DelayMs(500);
	}

	return 0;
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

