/* Copyright 2018, Eduardo Filomena - Gonzalo Cuenca
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Blinking Bare Metal example source file
 **
 ** This is a mini example of the CIAA Firmware.
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */

/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup Baremetal Bare Metal example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 *
 */	

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * yyyymmdd v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "system_init.h"
#include "max7219.h"
#include "led.h"
#include "switch.h"
#include "stopwatch.h"

void SysTick_Handler(void){
	static uint32_t cnt = 0;
	cnt = cnt % 333;
	if (cnt == 0){
		Led_Toggle(GREEN_LED);
	}
	cnt ++;
}
#define CHANGE_DELAY 100

int main(void)
{ 


	SystemInit();
	Init_Leds();
	Init_Switches();
	StopWatch_Init();
	SysTick_Config ( SystemCoreClock / 1000);

	max7219_t max7219;
	Max7219Init(&max7219,GPIO_0,max7219_spi_default_cfg);

	uint8_t key=0;
	while (1) {
		key = Read_Switches();
		/* Fills led matrix. One by one with delay */
		if (key == 1) {
			for (uint8_t y = 1; y <= MAX7219_SIZE; y++) {
				for (uint8_t x = 1; x <= MAX7219_SIZE; x++) {
					Max7219SetPoint(&max7219, x, y);
					Max7219Update(&max7219);
					StopWatch_DelayMs(CHANGE_DELAY);
				}
			}
		}
		/*  Clears led matrix. One by one with delay */
		if (key == 2) {
			for (uint8_t y = 1; y <= MAX7219_SIZE; y++) {
				for (uint8_t x = 1; x <= MAX7219_SIZE; x++) {
					Max7219ResetPoint(&max7219, x, y);
					Max7219Update(&max7219);
					StopWatch_DelayMs(CHANGE_DELAY);
				}
			}
		}
		/* Sets a stripes pattern */
		if (key == 4) {
			max7219Data_t data = { 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00 };
			Max7219SetImage(&max7219, data);
			Max7219Update(&max7219);
		}
		/* Clears led matrix. All led a the same time. No delay */
		if (key == 8) {
			Max7219Blank(&max7219);
			Max7219Update(&max7219);
		}
	}

	return 0;
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

