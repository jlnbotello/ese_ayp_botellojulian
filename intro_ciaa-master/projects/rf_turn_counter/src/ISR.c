/*==================[inclusions]=============================================*/

#include "ISR.h"
#include "chip.h"
#include "led.h"
#include "nrf24l01.h"
#include "gpio_hal.h"

/*==================[external functions definition]==========================*/

void SysTick_Handler(void){
	static uint32_t cnt = 0;
	cnt = cnt % 200;

	if (cnt == 0){
		Led_Toggle(RGB_R_LED);
#if asTX
		Nrf24TxTick(); /* Do transmission */
#endif
	}
	cnt ++;
}

#if asTX
void GPIO0_IRQHandler(void){
	NVIC_DisableIRQ( PIN_INT0_IRQn);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(0));
	GpioInterrupt(0); /* Tells the GPIO HAL who interrupt*/
	NVIC_EnableIRQ( PIN_INT0_IRQn);
}
#endif

#if asRX
void GPIO1_IRQHandler(void){
	NVIC_DisableIRQ( PIN_INT1_IRQn);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(1));
	GpioInterrupt(1); /* Tells the GPIO HAL who interrupt*/
	NVIC_EnableIRQ( PIN_INT1_IRQn);
}
#endif

